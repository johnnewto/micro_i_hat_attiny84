
#include <FiniteStateMachine.h>

/** this is the definitions of the button state machine **/
State btnOff = State( NULL,btnOffUpdate,NULL );  
State btnEdge = State(NULL,btnEdgeUpdate,NULL );  
State btnOn  = State( btnOnEnter, btnOnUpdate,  NULL );  

FSM smButton = FSM(btnOff);
bool buttonPress = false;

bool btnIsActive() {
  bool ret = false;
  if(digitalRead(BUTTON_PIN) == 0) {
     ret = true; 
  }  
  return ret;
}

void btnOffEnter() {
  //mySerial.println(F("btnOffEnter")); 
  pinMode(BUTTON_PIN, INPUT_PULLUP);
}

void btnOffUpdate() {
  if(btnIsActive()) {
    smButton.transitionTo(btnEdge);
  }
}

void btnEdgeEnter() {
 // mySerial.println(F("btnEdgeEnter")); 
}

void btnEdgeUpdate() {
  if(smButton.timeInCurrentState() > 500 && digitalRead(BUTTON_PIN) == 0) {
    smButton.transitionTo(btnOn);
  }

  if(!btnIsActive()) {
    smButton.transitionTo(btnOff);    
  }
}

void btnOnEnter() {
  //mySerial.println(F("buttonPress = true")); 
  buttonPress = true;
}

void btnOnUpdate() {
  if(!btnIsActive()) {
     smButton.transitionTo(btnOff); 
  }  
}


bool btnIsClicked() {
  bool ret = buttonPress;
  buttonPress = false;  
  return ret;
}


