
#include <FiniteStateMachine.h>

#define  POWER_DOWN_STATE_WAIT_TIME  20000   // msec ater issueing power down signal
#define  HEART_BEAT_WAIT_TIME        20000   // wait time for heart beat signal
#define  BOOT_STATE_WAIT_TIME        60000   // msec ater issueing power on signal

// Routines to set and clear bits (used in the sleep code)
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

/** this is the definitions of the power state machine **/
State pwrOff      = State( pwrOffEntry,pwrOffUpdate,NULL );  
State pwrBoot     = State(pwrBootEntry,pwrBootUpdate,NULL );  
State pwrOn       = State( pwrOnEntry, pwrOnUpdate,  NULL );  
State pwrDown     = State( pwrDownEntry, pwrDownUpdate,  NULL );  

FSM smPwr = FSM(pwrOff);

void enableSMPS(bool on) {
  if(on) {
    digitalWrite(PS_EN_Pin, HIGH);
    digitalWrite(PWR_EN_PIN, LOW) ;
  }
  else {
    digitalWrite(PS_EN_Pin, LOW);
    digitalWrite(PWR_EN_PIN, HIGH);
  }
}void pwrOffEntry() { 
//  e:Disable PS
//  e:swLED On
  enableSMPS(false);
//  mySerial.println(F("pwrOffEntry"));
}

void pwrOffUpdate() {
//  u:swLED Off
  smLED.immediateTransitionTo(ledOff);
 // mySerial.println(F("pwrOffUpdate"));

// Long keypress -> pwrBoot
  if(btnIsClicked()) {


    //mySerial.println(F("transitionTo(pwrBoot)"));
    BatteryResetVolts();
    smPwr.transitionTo(pwrBoot);
 }
 
//  u:Atiny84 Sleep
  if (ENABLE_WDT) { 
    if(!btnIsActive()) {
      //mySerial.println(F("Sleep"));
      pwrSystemSleep();  // start power down sequence
    }
  }
}


void pwrBootEntry() { 
//  e:Enable PS
  enableSMPS(true);
  mySerial.println(F("pwrBootEntry"));
//  e:Flash swLED fast
  smLED.transitionTo(ledFlashOn);
  ledSetFlash(200,200, LED_RED_GREEN);  
}

void pwrBootUpdate() {

// xxx seconds -> pwrOff
// rPi BootOK -> pwrOn

//****************  uncomment this if you want boot to time out **********************
//  if(smPwr.timeInCurrentState() > BOOT_STATE_WAIT_TIME) {
//    smPwr.transitionTo(pwrOff);    
//  }

  if(btnIsClicked()) {
    // immediate transition as pwrRPiHeartBeat() could overide this
    smPwr.immediateTransitionTo(pwrOff);
  }

  if(BatteryReadVolts() < BAT_LOW_VOLTS) {  
    // immediate transition as pwrRPiHeartBeat() could overide this
    mySerial.println(F("Battery Low"));
    smPwr.transitionTo(pwrOff);
  }
}

void pwrOnEntry() { 
  // e:swLED On
  smLED.transitionTo(ledOn);
  //  mySerial.println(F("pwrOnEntry"));
}
void pwrOnUpdate() { 
  // 30 seconds -> wait for hear beat
  if(smPwr.timeInCurrentState() > HEART_BEAT_WAIT_TIME) {
    mySerial.println(F("Heartbeat Timeout"));
    smPwr.transitionTo(pwrDown);     
  }
  if(btnIsClicked()) {
    // immediate transition as pwrRPiHeartBeat() could overide this
    mySerial.println(F("btnIsClicked"));
    smPwr.immediateTransitionTo(pwrDown);
  }
  
  if(BatteryReadVolts() < BAT_LOW_VOLTS) {  // 
    // immediate transition as pwrRPiHeartBeat() could overide this
    mySerial.println(F("Battery Low"));
    smPwr.immediateTransitionTo(pwrDown);
  }
}

void pwrDownEntry() {
//  e:Signal rPi power off
//  e:Flash swLED Slow
  mySerial.println(F("OFF"));
  ledSetFlash(100,900, LED_RED);  
  smLED.transitionTo(ledFlashOff);
 
  smPwr.resetTimer();
}

void pwrDownUpdate() {
  //  30 seconds or rPi power off signal -> PwrOff
  if(smPwr.timeInCurrentState() > POWER_DOWN_STATE_WAIT_TIME) {
    smPwr.transitionTo(pwrOff);    
  }
  if(smPwr.getTimer() > 1000) {
    mySerial.println(F("OFF"));
    smPwr.resetTimer();
  }

}

void pwrRPiHeartBeat() {
  // Raspberry pi to issue Heartbeat signal if active (not crashed)
  
  if (smPwr.isInState(pwrOn) == true || smPwr.isInState(pwrBoot) == true) {
    smPwr.immediateTransitionTo(pwrOn);
    ledSetFlash(10000,10, LED_GREEN);   // long period so to show heartbeat
    smLED.transitionTo(ledFlashOff);
  }
}

void pwrRpiPowerDown() {
  // Raspberry pi can issue down signal
  if (smPwr.isInState(pwrOff) != true) {
    smPwr.immediateTransitionTo(pwrDown);
  }
}

void pwrResetWatchdog(int ii) 
{
  // 0=16ms, 1=32ms,2=64ms,3=128ms,4=250ms,5=500ms
  // 6=1 sec,7=2 sec, 8=4 sec, 9= 8sec

  uint8_t bb;
  if (ii > 9 ) ii=9;
  bb=ii & 7;
  if (ii > 7) bb|= (1<<5);
  bb|= (1<<WDCE);

  MCUSR &= ~(1<<WDRF);
  // start timed sequence
  WDTCSR |= (1<<WDCE) | (1<<WDE);
  // set new watchdog timeout value
  WDTCSR = bb;
  WDTCSR |= _BV(WDIE);
}


// system wakes up when watchdog is timed out of pin change
void pwrSystemSleep() 
{
  cbi(ADCSRA,ADEN);                    // switch Analog to Digitalconverter OFF
  pwrResetWatchdog(9);                 // approximately 8 seconds sleep, then wakeup
 
  set_sleep_mode(SLEEP_MODE_PWR_DOWN); // sleep mode is set here
  sleep_enable();
  sei();                               // Enable the Interrupts so the wdt can wake us up
  
  digitalWrite(Tx, LOW); // otherwise will be high on powerdown drawing current

  sleep_mode();                        // System sleeps here

  sleep_disable();                     // System continues execution here when watchdog timed out 
  sbi(ADCSRA,ADEN);                    // switch Analog to Digitalconverter ON
}




