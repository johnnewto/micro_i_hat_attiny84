// Save memory by using the progmem for strings
// http://arduiniana.org/libraries/flash/
//#include <Flash.h>
#include <EEPROM.h>

#define newLineStr "\r\n"
int unlock = 0;

void serviceSerial() {

  if (mySerial.available() > 0)
  {
    // read the incoming byte:
    int incomingByte = mySerial.read();
    int cnt=0;
    int i=0;
    int tmr;
    int offset = 1;
    char chr;
    char buffer[20];
    
    switch(incomingByte)
    {    

    case 'H':  
    case 'h':  
      pwrRPiHeartBeat();
      break;  
         
    case 'S': 
    case 's':  
      while (mySerial.available()) {
        pwrResetWatchdog(3);      // reset Watchdog  128ms
        buffer[cnt] = mySerial.read();
        cnt++;
        if (cnt > 20) break;
      }
      for (i=0;i<cnt; i++) {
        pwrResetWatchdog(3);      // reset Watchdog  128ms 
        mySerial.print(buffer[i]);       
      }     
      mySerial.println(':');        
      break; 
      
    case '!':
//      mySerial.println(F("Unlocking"));
      unlock = __testUnlock();
      break;

    case 'l':
      if (unlock == 4) {
        mySerial.println(F("Unlocked = True"));
      } else  {
        mySerial.println(F("Unlocked = False"));        
      }
      unlock = 0;   
      break;

    case 'W':
      offset = 21;     
    case 'w': 
      if (unlock == 4) {
        while (mySerial.available()) {
          pwrResetWatchdog(3);      // reset Watchdog  128ms 
          chr = mySerial.read();
          EEPROM.write(cnt+offset, chr);
          cnt++;
          if (cnt > 20) break;
        }  
      }
      unlock = 0;                   
      break;     
         
    case 'R': 
    case 'r':  
      for (i=0;i<40; i++) {
        pwrResetWatchdog(3);      // reset Watchdog  128ms 
        chr = EEPROM.read(i+offset); 
        mySerial.print(chr);       
      }
      mySerial.println(':');       
      
      break;  
                    
    case 'c':
    case 'C':
      if (unlock == 4) {  
        BatteryCalibrate();
      } else {
        mySerial.print(F("Read calibration ")); 
        mySerial.println(EEPROM.read(0));   
      }     

      unlock = 0;
      break;
      
    case 'V': 
    case 'v':  
      i = BatteryReadVolts();
      mySerial.print(i);             
      break;               
                   
    default:
      unlock = 0;
      break;
    }

    mySerial.flush();
  }
}


int __testUnlock() {    
  int cnt = 1;
  char chr;  
  while (mySerial.available()) {
    pwrResetWatchdog(3);      // reset Watchdog  128ms 
    chr = mySerial.read();
    switch(chr)
    {
    case '@':      
      if (cnt == 1) {
//        mySerial.println(F("Unlocking"));        
        cnt++;
      }
      break;    
  
    case '#':     
      if (cnt == 2){ 
//        mySerial.println(F("Unlocking"));        
        cnt++;
      }
      break;    
      
    case '$':      
      if (cnt == 3){ 
//        mySerial.println(F("Unlocking"));        
        cnt++;
      }
      break;
      
    default:
      cnt = 0;
    }
  }
  return cnt;
}



