#include <FiniteStateMachine.h>
#include <MovingAverageFilter.h>
#include <EEPROM.h>

// Routines to set and clear bits (used in the sleep code)
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif


#define  BAT_LOW_VOLTS               280     // battery volts  = 2.8 x 100 where it will power down
#define  BAT_GOOD_VOLTS              420     // 4.2 volts x 100
#define  BAT_CAL_VOLTS               400     // 4.0 volts x 100  (new change)
#define  BAT_CAL_VOLTSX10000         40000   // 4.0 volts x 100

/** this is the definitions of the power state machine **/
State batSense    = State( batSenseEntry,batSenseUpdate,NULL );  



FSM smBattery = FSM(batSense);

MovingAverageFilter movingAverageFilter(8, BAT_CAL_VOLTS);  
long batVoltageX100;    //  use interger times 100 voltage 
long calibration;        // calibration value, nominally 100

int counter = 0;

void batSenseEntry() {
// e:measure bat Volts
//  e:toggle green LED
//  e:log current & bat volts

  calibration = BatteryReadCalibration();
  BatteryResetVolts();
  mySerial.print(F("Battery cal value "));
  mySerial.println(calibration);
}

void batSenseUpdate() {
//  10 seconds -> BatterySense
//  Bat Low -> PwrDown
  if(smBattery.timeInCurrentState() > 1000) {
    long val = __BatteryReadVolts();
    batVoltageX100 = movingAverageFilter.process(val);
    if(counter++ > 5) {
      mySerial.print(F("Vlipo "));
      mySerial.println(batVoltageX100);
      counter = 0;
    }
    smBattery.transitionTo(batSense);  // transition to itself
  }
}

long __BatteryReadVolts(){
  long val = 1127 * calibration / analogRead_alt(); // 112700 = 1.1volt x1024 * 100
  return val;
}

int BatteryReadVolts(){
  return batVoltageX100;
}


long  BatteryReadCalibration() {
    int cal = EEPROM.read(0);
    if (cal > 90 && cal < 110) {
      return cal;
    }
    else
      return 100;
}

void BatteryResetVolts() {

    for(int i=0; i<10; i++) {
      batVoltageX100 = movingAverageFilter.process(BAT_CAL_VOLTS);  // preset filter to BAT_GOOD_VOLTS = 420
    }
}

void BatteryCalibrate() {
  // expect read value of 420  = 4.2 volts on the batt in
  // the ideal calibration value is 100, 
  calibration = 100;
  batVoltageX100 = __BatteryReadVolts();
  calibration = BAT_CAL_VOLTSX10000 / batVoltageX100;   // bug as (BAT_CAL_VOLTS*100) / batVoltageX100 gives neg value
 if (calibration > 90 && calibration < 110) {
    EEPROM.write(0, calibration);
    mySerial.print(F("Success "));
  }else{
    mySerial.print(F("Fail *** "));
  }
  mySerial.print(F("Write calibration ")); 
  mySerial.print(calibration );
   


}



