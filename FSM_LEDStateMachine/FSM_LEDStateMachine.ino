//
// Note: THE micro IS an ATtiny84, not ATtiny841
//
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>

#include <FiniteStateMachine.h>
#include "SoftwareSerial.h"
//

// if true runs a "forever" while loop on keypress to test WDT timeout reset 
#define ENABLE_WDT true
 

const int LED_BTN_PIN_2 = 2;   // PA2      
const int LED_BTN_PIN_1 = 1;   // PA1      
const int BUTTON_PIN  = 3;    // PA3
const int SCLK_PIN = 4;       // PA4   

//const int ANALOG_VLIPO = A1;        // const does not work


#if defined (__AVR_ATtiny84__)
const int Rx = 9;           // physical pin PB1 on ATiny84 Yellow
const int Tx = 8;           // physical pin PB2 on ATiny84 Orange
const int PS_EN_Pin = 7;   // physical pin PA7 on ATiny84
const int PWR_EN_PIN = 10;  // physical pin PB0 on ATiny84
#else
const int Tx = 11; // this is physical pin 11 on Arduino Uno
const int Rx = 12; // this is physical pin 12 on Arduino Uno
const int PS_EN_Pin = 13;       // this is actually the LED
const int PWR_EN_PIN = 10;  // physical pin 10 on Arduino Uno
#endif

#define versionStr "Hat-power v1.0"

SoftwareSerial mySerial(Rx, Tx);

extern FSM smButton;
extern FSM smLED;
extern FSM smPwr;
extern FSM smBattery;


int analogPin = 0;     //PA0
int val = 0;

void setup(){ 
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(SCLK_PIN, INPUT);
  pinMode(Rx, INPUT);
  pinMode(Tx, OUTPUT);
  //analogReference(INTERNAL);   // was INTERNAL = 2
  analog_setup();

  pinMode(LED_BTN_PIN_1, OUTPUT);
  pinMode(LED_BTN_PIN_2, OUTPUT);
  pinMode(PS_EN_Pin, OUTPUT);
  pinMode(PWR_EN_PIN, OUTPUT);
  
  // Pint 3 interrupt
  PCMSK0 = 0b00001000;     // PCINT3 = LED_BTN_PIN interupt
  GIFR   = 0b00000000;     // clear any outstanding interrupts
  GIMSK  = 0b00010000;     // enable pin change interrupts PCIE0=1
  //  SREG   = 0b10000000;   // enable SREG I-bit
  sei();
  mySerial.begin(4800, 1.05);   // needs calibrating JN
  mySerial.println(F(versionStr));
}


void loop(){
  serviceSerial();  
  smLED.update();
  smButton.update();
  smBattery.update();
  smPwr.update();  
  pwrResetWatchdog(3);      // reset Watchdog  128ms if in this loop
}


