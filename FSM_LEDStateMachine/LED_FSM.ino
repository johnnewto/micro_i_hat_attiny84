
#include <FiniteStateMachine.h>

//const byte LED_PIN = 13; 
//int blinkPin = 2;
//int PSPin = 10;
//int BUTTON_PIN = 3;   // PA3

//int LEDPin = 7;
 
#define LED_OFF 0
#define LED_RED 1
#define LED_GREEN 2
#define LED_RED_GREEN 3

/** this is the definitions of the states that our program uses */
State ledOn = State(ledOnEntry, NULL, NULL);  //this state fades the LEDs in
State ledFlashOn = State(ledFlashOnEntry, ledFlashOnUpdate, NULL);  //this state flashes the leds FLASH_ITERATIONS times at 1000/FLASH_INTERVAL
State ledFlashOff = State(ledFlashOffEntry, ledFlashOffUpdate, NULL);  //this state flashes the leds FLASH_ITERATIONS times at 1000/FLASH_INTERVAL
State ledOff = State(ledOffEntry, NULL, NULL);  //this state fades the LEDs in

/** the state machine controls which of the states get attention and execution time */
FSM smLED = FSM(ledOff); //initialize state machine, start in state: ledOff

int ledFlashOnTime = 500;
int ledFlashOffTime = 500;
int ledFlashColor = LED_OFF;

void setSwitchLed(byte color) {
  switch(color) {
    case LED_OFF:
      digitalWrite(LED_BTN_PIN_1, LOW);     
      digitalWrite(LED_BTN_PIN_2, LOW);     
      break;
    case LED_GREEN:
      digitalWrite(LED_BTN_PIN_1, HIGH);     
      digitalWrite(LED_BTN_PIN_2, LOW);      
      break;
    case LED_RED:
    case LED_RED_GREEN:
      digitalWrite(LED_BTN_PIN_1, LOW);     
      digitalWrite(LED_BTN_PIN_2, HIGH);     
      break;
  }
}

void ledOnEntry() {
  //Serial.println(F("ledOnEntry")); 
  setSwitchLed(ledFlashColor); 
}

void ledOffEntry() {
  //Serial.println(F("ledOffEntry")); 
  setSwitchLed(LED_OFF);
}

void ledFlashOnEntry() {
  //Serial.println(F("ledFlashOnEntry"));   
  setSwitchLed(ledFlashColor);
}

void ledFlashOnUpdate() {
  if( smLED.timeInCurrentState() > ledFlashOnTime) {
      smLED.transitionTo(ledFlashOff);
  }
}

void ledFlashOffEntry() {
  //Serial.println(F("ledFlashOffEntry"));   
  if(ledFlashColor == LED_RED_GREEN) {
    setSwitchLed(LED_GREEN);  // flash green instead of off
    //mySerial.println(F("led Green"));
  } else {
    setSwitchLed(LED_OFF);
    //mySerial.println(F("led Off"));
  }  
}

void ledFlashOffUpdate() {
  if( smLED.timeInCurrentState() > ledFlashOffTime) {
      smLED.transitionTo(ledFlashOn);
  }
}


void ledSetFlash(int onTime, int offTime, int color) {
  ledFlashOnTime = onTime;
  ledFlashOffTime = offTime;
  ledFlashColor = color;
}

